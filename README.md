Pre-Build an Executable splitsh lite Image
==========================================

Build a working copy of the [splitsh-lite](https://github.com/splitsh/lite) utility from
source, using a fork that preserves the early histories of subtrees within the monolithic
repository that were once freestanding repositories.

The naive version expects to be used with the directory holding the Git repository added
as a volume to the container, for example:

    git clone https://github.com/twigphp/Twig
    docker run -v $(pwd):/repo splitting-image --prefix=lib/ --origin=refs/tags/v1.24.1 --path=Twig --scratch

Where `-v $(pwd):/repo` adds the current directory as the volume, `splitting-image` is the image name, and the rest of the line contains arguments for splitsh-lite.
