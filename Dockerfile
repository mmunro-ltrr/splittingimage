# The git2go library needs the following version of golang
# (keep checking what's working or broken at https://travis-ci.org/libgit2/git2go):
FROM golang:1.7.5

# We need cmake to run the git2go build script, and the SSL & SSH
# libraries for a completely working libgit2; note that a final statically
# linked binary would be nice, but the usual recipes for this don't work here.
RUN apt-get update && \
  apt-get install -y \
    cmake \
    libssl-dev \
    libssh2-1-dev \
  && rm -r /var/lib/apt/lists/* \
  && cd "$GOPATH" \
  && go get github.com/boltdb/bolt \
  && git clone https://github.com/libgit2/git2go.git "$GOPATH/src/github.com/libgit2/git2go" \
  && cd "$GOPATH/src/github.com/libgit2/git2go" \
  && git submodule update --init \
  && make install \
  && git clone -b UADIGITAL-739 https://github.com/ltrr-arizona-edu/lite "$GOPATH/src/github.com/splitsh/lite" \
  && cd "$GOPATH" \
  && go build github.com/splitsh/lite \
  && go install github.com/splitsh/lite

# The working directory containing the Git repository to process.
VOLUME ["/repo"]
WORKDIR /repo

ENTRYPOINT ["/go/bin/lite"]